from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from .serializers import CategorySerializer
from .models import Category
# Create your views here.


class CategoryViewMode(ReadOnlyModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    lookup_url_kwarg = 'category_id'
