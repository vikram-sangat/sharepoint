from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from .models import Category
from apps.files.models import File


class FileInline(admin.TabularInline):
    model = File


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category_id', 'name', 'color',
                    'parent', 'is_subcategory', 'created_at')

    search_fields = ('name', 'parent__name')


admin.site.register(Category, CategoryAdmin)
