# Generated by Django 3.1.7 on 2021-03-13 07:55

import apps.core.funtions
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('category_id', models.BigIntegerField(default=apps.core.funtions.generateUUID, editable=False, primary_key=True, serialize=False, unique=True, verbose_name='category id')),
                ('category_name', models.CharField(max_length=200, verbose_name='category name')),
                ('created_at', models.DateTimeField(default=datetime.datetime.utcnow, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
    ]
