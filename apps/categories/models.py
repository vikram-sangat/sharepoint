from django.db import models
from datetime import datetime
from apps.core.funtions import generateUUID
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here.


class Category(models.Model):
    category_id = models.BigIntegerField(
        ('category id'), default=generateUUID, primary_key=True, unique=True, editable=False)

    name = models.CharField(('category name'), max_length=200)
    color = models.CharField(('category color'), max_length=20, default='')
    parent = models.ForeignKey('self', to_field='category_id',
                               default=None, null=True, blank=True, on_delete=models.SET_NULL)
    is_subcategory = models.BooleanField(
        "is subcategory", default=False, blank=True)
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)
    file = models.ForeignKey('files.File', to_field='file_id',
                             on_delete=models.SET_NULL, blank=True, null=True, default=None)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return f'{self.name}'
