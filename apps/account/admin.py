from django.contrib import admin
from .models import Referral, SocialMediaAccount, AccountActivationCode


class ReferralAdmin(admin.ModelAdmin):
    list_display = ('referral_id', 'user',
                    'referral_code', 'created_at')


class SocialMediaAdmin(admin.ModelAdmin):
    pass


class AccountActivationAdmin(admin.ModelAdmin):
    list_display = ('activation_id', 'user',
                    'activation_code', 'is_used', 'expires_at', 'created_at')


admin.site.register(Referral, ReferralAdmin)
admin.site.register(SocialMediaAccount, SocialMediaAdmin)
admin.site.register(AccountActivationCode, AccountActivationAdmin)
