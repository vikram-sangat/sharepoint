# Generated by Django 3.1.7 on 2021-03-21 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0003_productimages_type'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productimages',
            old_name='desription',
            new_name='description',
        ),
        migrations.AlterField(
            model_name='product',
            name='condition',
            field=models.CharField(choices=[('EXCELLENT', 'Excellent'), ('GOOD', 'Good'), ('SATISFACTORY', 'Satisfactory'), ('BAD', 'Bad')], db_index=True, default='SATISFACTORY', max_length=255, verbose_name='condition'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price_range',
            field=models.CharField(choices=[('ZERO_TO_FIVEHUNDRED', 'Zero To Fivehundred'), ('FIVEHUNDRED_TO_ONETHOUSAND', 'Fivehundred To Onethousand'), ('ONETHOUSAND_TO_ONETHOUSANDFIVEHUNDRED', 'Onethousand To Onethousandfivehundred'), ('ONETHOUSANDFIVEHUNDRED_TO_TWOTHOUSAND', 'Onethousandfivehundred To Twothousand'), ('TWOTHOUSAND_TO_TWOTHOUSANDFIVEHUNDRED', 'Twothousand To Twothousandfivehundred'), ('TWOTHOUSANDFIVEHUNDRED_TO_THREETHOUSAND', 'Twothousandfivehundred To Threethousand'), ('THREETHOUSAND_TO_THREETHOUSANDFIVEHUNDRED', 'Threethousand To Threethousandfivehundred'), ('THREETHOUSANDFIVEHUNDRED_TO_FOURTHOUSAND', 'Threethousandfivehundred To Fourthousand'), ('FOURTHOUSAND_TO_FOURTHOUSANDFIVEHUNDRED', 'Fourthousand To Fourthousandfivehundred'), ('FOURTHOUSANDFIVEHUNDRED_TO_FIVETHOUSAND', 'Fourthousandfivehundred To Fivethousand'), ('FIVETHOUSAND_TO_TENTHOUSAND', 'Fivethousand To Tenthousand'), ('TENTHOUSAND_TO_THIRTYTHOUSANDPLUS', 'Tenthousand To Thirtythousandplus'), ('UNKNOWN', 'Unknown')], db_index=True, default='UNKNOWN', max_length=255, verbose_name='price range'),
        ),
    ]
