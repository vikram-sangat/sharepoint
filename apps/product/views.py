from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
import traceback
from rest_framework import status, exceptions
from django.utils import datetime_safe
from django.db import transaction
from rest_framework.decorators import action
from .serializers import ProductSerializer, ProductCreateSerializer
from apps.categories.models import Category
from apps.files.models import File
from .models import Product, ProductStatus, ImageType, ProductImages

# Create your views here.


class ProductViewMode(ReadOnlyModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.filter(status=ProductStatus.APPROVED)
    lookup_url_kwarg = 'product_id'
    filterset_fields = [
        'sub_category__name', 'category__name', 'owner', 'manufacturer']
    search_fields = ['title', 'description',
                     'category__name']

    @action(detail=False, url_path='user')
    def user_products(self, request):
        products = Product.objects.filter(
            owner=request.user).order_by('-created_at')
        products = self.filter_queryset(products)
        page = self.paginate_queryset(products)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(products, many=True)
        return Response(serializer.data)


class ProductCreate(CreateAPIView):
    serializer_class = ProductCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data
        with transaction.atomic():
            images = data.pop('files', [])
            category_id = data.pop('category_id', 0)
            sub_category_id = data.pop('sub_category_id', 0)
            try:
                category = Category.objects.get(category_id=category_id)
            except:
                traceback.print_exc()
                raise exceptions.ParseError(
                    f'Category {category_id} not found.')
            sub_category = None
            if sub_category_id != 0:
                try:
                    sub_category = Category.objects.get(
                        category_id=sub_category_id)
                except:
                    traceback.print_exc()
                    raise exceptions.ParseError(
                        f'Sub categoryCategory {sub_category_id} not found.')

            product = Product.objects.create(
                **data,
                category=category,
                sub_category=sub_category,
                owner=request.user,
                status=ProductStatus.REVIEW,
                status_change_date=datetime_safe.datetime.now())

            for image in images:
                file_id = image.get('file_id', 0)
                description = image.get('description', '')
                imageType = image.get('type', ImageType.IMAGE)
                try:
                    file = File.objects.get(file_id=file_id)
                except:
                    traceback.print_exc()
                    raise exceptions.ParseError(f'File {file_id} not found.')
                ProductImages.objects.create(
                    file=file,
                    product=product,
                    description=description,
                    type=imageType
                )

            return Response(status=status.HTTP_200_OK)
