from rest_framework import serializers

from .models import Product
from apps.files.serializers import FileSerializer
from apps.user.serializers import UserSerializer
from apps.categories.serializers import CategorySerializer
from .models import ProductImages, ImageType


class ProductImageSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    file = FileSerializer()

    class Meta:
        model = ProductImages
        fields = ['file', 'description', 'type']


class ProductSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    images = ProductImageSerializer(
        source='productimages_set', many=True)
    owner = UserSerializer()
    borrower = UserSerializer()
    category = CategorySerializer()
    sub_category = CategorySerializer()

    class Meta:
        model = Product
        fields = '__all__'


class ProductDetailSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    images = ProductImageSerializer(
        source='productimages_set', many=True)
    owner = UserSerializer()
    borrower = UserSerializer()
    category = CategorySerializer()
    sub_category = CategorySerializer()

    class Meta:
        model = Product
        fields = '__all__'


class ProductCreateImageSerializer(serializers.Serializer):
    """
    Serializer which outputs user data.
    """
    file_id = serializers.IntegerField()
    description = serializers.CharField()
    type = serializers.ChoiceField(choices=ImageType.choices)


class ProductCreateSerializer(serializers.ModelSerializer):
    """
    Serializer which outputs user data.
    """
    files = ProductCreateImageSerializer(many=True, required=True)
    category_id = serializers.IntegerField(required=True)
    sub_category_id = serializers.IntegerField(required=False)

    class Meta:
        model = Product
        exclude = ['owner', 'borrower', 'status', 'images',
                   'status_change_date', 'created_at', 'category', 'sub_category']
