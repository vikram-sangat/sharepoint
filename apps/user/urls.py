from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('info', views.UserInfo.as_view(), name='userInfo'),
    path('register', views.UserRegister.as_view(), name='userRegister'),
    path('update', views.UpdateUser.as_view(), name='userUpdate'),
]
