from django.urls import path
from . import views

urlpatterns = [
    path('social/login/', views.Login.as_view(), name='socialLogin'),
    path('authorize/', views.LoginUser.as_view(), name='login'),
    path('logout/', views.LogoutUser.as_view(), name='logout'),
]
