
import base64
import binascii

from django.contrib.auth import authenticate, get_user_model
from django.middleware.csrf import CsrfViewMiddleware
from django.utils.translation import gettext_lazy as _
from .models import Session
from django.conf import settings
import jwt
import traceback

from rest_framework import HTTP_HEADER_ENCODING, exceptions
from rest_framework.authentication import BaseAuthentication


def get_authorization_header(request):
    """
    Return request's 'Authorization:' header, as a bytestring.

    Hide some test client ickyness where the header can be unicode.
    """
    auth = request.META.get('HTTP_AUTHORIZATION', b'')
    if isinstance(auth, str):
        # Work around django test client oddness
        auth = auth.encode(HTTP_HEADER_ENCODING)
    return auth


class BearerAuthentication(BaseAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:

        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Bearer'
    model = None

    def get_model(self):
        if self.model is not None:
            return self.model
        from rest_framework.authtoken.models import Token
        return Token

    """
    A custom token model may be used, but must have the following properties.

    * key -- The string identifying the token
    * user -- The user to which the token belongs
    """

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _(
                'Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_token(token)

    def authenticate_token(self, token):
        initialToken = jwt.decode(token, options={"verify_signature": False})
        decoded = {}
        try:
            decoded = jwt.decode(f'{token}', settings.ISSUER, audience=settings.ISSUER, issuer=settings.ISSUER,  options={
                                 "require": ["exp", "iss", "sub"]}, algorithms=["HS256"])
            return self.authenticate_credentials(decoded.get('id', ''), decoded.get('sub', ''))
        except jwt.ExpiredSignatureError:
            traceback.print_exc()

            session = Session.objects.filter(
                session_id=initialToken.get('id', '')).first()
            if session is not None:
                session.delete()
            raise exceptions.AuthenticationFailed(_('Invalid token.'))
        except:
            traceback.print_exc()
            raise exceptions.AuthenticationFailed(_('Invalid token.'))
        return decoded

    def authenticate_credentials(self, token, userid):
        user = get_user_model()
        try:
            session = Session.objects.get(
                session_id=token)
        except Session.DoesNotExist:
            traceback.print_exc()

            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        try:
            current_user = user.objects.get(
                user_id=userid)
        except user.DoesNotExist:
            traceback.print_exc()

            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        if not current_user.is_active:
            raise exceptions.AuthenticationFailed(
                _('User inactive or deleted.'))

        return (current_user, session)

    def authenticate_header(self, request):
        return self.keyword
