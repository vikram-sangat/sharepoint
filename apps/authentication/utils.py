from datetime import datetime, timedelta
import jwt
from django.conf import settings
from django.contrib.auth.models import User
from .models import Session


def generateAccessToken(user: User, tokenID: str):

    today = datetime.today()
    accessTokenexpiry = today+timedelta(days=2)
    refreshTokenexpiry = today+timedelta(days=2)
    ndf = datetime.utcnow().timestamp()
    iat = ndf
    accessTokenClaims = {
        "exp": accessTokenexpiry.timestamp(),
        "nbf": ndf,
        "iss": settings.ISSUER,
        "id": tokenID,
        "aud": [settings.ISSUER],
        "iat": ndf,
        "sub": user.user_id
    }
    refreshTokeClaims = {
        "exp": refreshTokenexpiry.timestamp(),
        "nbf": ndf,
        "iss": settings.ISSUER,
        "id": tokenID,
        "aud": [settings.ISSUER],
        "iat": ndf,
        "sub": user.user_id
    }
    accessTokenJWT = f'Bearer {jwt.encode(accessTokenClaims, settings.ISSUER)}'
    refreshTokenJWT = f'Bearer {jwt.encode(refreshTokeClaims, settings.ISSUER)}'
    session = Session.objects.create(
        user=user,
        session_id=tokenID, access_token=accessTokenJWT, refresh_token=refreshTokenJWT, expire_at=accessTokenexpiry)
    return accessTokenJWT, refreshTokenJWT
