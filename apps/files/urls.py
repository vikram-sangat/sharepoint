from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'', views.FileViewModel, basename='files')

urlpatterns = [
    path('upload/', views.UploadFile.as_view(), name='upload_file'),
    path('', include(router.urls), name='files'),
]
