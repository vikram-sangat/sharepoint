from django.db import models
from datetime import datetime
from apps.core.funtions import generateUUID
from django.contrib.auth import get_user_model


class File(models.Model):
    file_id = models.BigIntegerField(
        ('file id'), default=generateUUID, primary_key=True, unique=True, editable=False)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_NULL, null=True, to_field='user_id', related_query_name='created_by')
    file_path = models.FileField(upload_to='user_uploads/%Y/%m/%d/')
    created_at = models.DateTimeField(('created at'), default=datetime.now)
    updated_at = models.DateTimeField(
        ('updated at'), auto_now=True)

    def __str__(self):
        return f'{self.file_id}'
